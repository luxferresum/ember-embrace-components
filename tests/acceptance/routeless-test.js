import { module, test } from 'qunit';
import { visit, currentURL } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';

module('Acceptance | routeless', function(hooks) {
  setupApplicationTest(hooks);

  test('visiting /', async function(assert) {
    await visit('/');
    assert.dom('[data-test-application-route]').exists();
  });

  test('visiting /my-route', async function(assert) {
    await visit('/my-route');
    assert.dom('[data-test-application-yield] [data-test-my-route]').exists();
    assert.dom('[data-test-my-route-model-info]').hasText('this is the dummy model');
  });

  test('visiting /parent/child', async function(assert) {
    await visit('/parent/child');
    assert.dom('[data-test-application-yield] [data-test-child]').exists();
  });
});
