import { module, test } from 'qunit';
import { visit, currentURL, fillIn, click } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';

module('Acceptance | query params', function(hooks) {
  setupApplicationTest(hooks);

  test('testing text', async function(assert) {
    await visit('/with-query-params');
    assert.equal(currentURL(), '/with-query-params');
    assert.dom('[data-query-text]').hasValue('hello');
    await fillIn('[data-query-text]', 'world');
    assert.equal(currentURL(), '/with-query-params?queryText=world');

    await visit('/with-query-params?queryText=mars');
    assert.dom('[data-query-text]').hasValue('mars');
  });

  test('testing bool', async function(assert) {
    await visit('/with-query-params');
    assert.equal(currentURL(), '/with-query-params');
    assert.dom('[data-query-bool]').hasText('false');
    await click('[data-query-bool-toggle]');
    assert.equal(currentURL(), '/with-query-params?queryBool=true');
    assert.dom('[data-query-bool]').hasText('true');

    await click('[data-query-bool-toggle]');
    assert.dom('[data-query-bool]').hasText('false');
    await visit('/with-query-params?queryBool=true');
    assert.dom('[data-query-bool]').hasText('true');
  });

  test('testing number', async function(assert) {
    await visit('/with-query-params');
    assert.dom('[data-query-number-plus-one]').hasText('13');

    await visit('/with-query-params?queryNum=41');
    assert.equal(currentURL(), '/with-query-params?queryNum=41');
    assert.dom('[data-query-number-plus-one]').hasText('42');
  });

  test('testing array', async function(assert) {
    await visit('/with-query-params');
    assert.equal(currentURL(), '/with-query-params');
    assert.dom('[data-list-query-arr] li').exists({ count: 1 });
    assert.dom('[data-list-query-arr] li').hasText('one');

    await click('[data-push-to-arr]');
    assert.equal(currentURL(), '/with-query-params?queryArr=%5B%22one%22%2C%22new%22%5D');
    assert.dom('[data-list-query-arr] li').exists({ count: 2 });
  });
});
