import Route from '@ember/routing/route';

export default class MyRouteRoute extends Route {
  model() {
    return { info: "this is the dummy model" };
  }
}
