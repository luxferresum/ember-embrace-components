import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class WithQueryParamsComponent extends Component {
  get numPlusOne() {
    return this.args.queryParams.queryNum + 1;
  }

  @action toggleBool() {
    this.args.queryParams.queryBool = !this.args.queryParams.queryBool;
  }

  @action pushToArr() {
    this.args.queryParams.queryArr.pushObject('new');
  }
}
