import Controller from '@ember/controller';

export default function(options) {
  const queryParams = options.queryParams ?? {};

  // we use classic style classes because thats an easy way to set the default query params
  // because we can just use splat notation.
  const ControllerWithQueryParams = Controller.extend({
    queryParams: Object.keys(queryParams),
    ...queryParams,
  });

  // here we use native classes because we can
  return class FakeController extends ControllerWithQueryParams {
    get queryParamsAccessObject() {
      return makeQueryParamsAccessObject(Object.keys(queryParams), this);
    }
  }
}

function makeQueryParamsAccessObject(queryParams, controller) {
  const obj = {};

  const queryParamsDefinition = Object.fromEntries(queryParams.map(queryParam => [queryParam, {
    enumerable: true,
    get() {
      return controller.get(queryParam);
    },
    set(val) {
      controller.set(queryParam, val);
    },
  }]))

  Object.defineProperties(obj, queryParamsDefinition);
  return obj;
}
